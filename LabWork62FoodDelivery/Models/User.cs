﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace LabWork62FoodDelivery.Models
{
    public class User : IdentityUser
    {
        public IEnumerable<Basket> Baskets { get; set; }
    }
}
