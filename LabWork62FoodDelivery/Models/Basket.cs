﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace LabWork62FoodDelivery.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public int Count { get; set; }
        public double Price { get; set; }
        public Dish Dish { get; set; }
        public int DishId { get; set; }
        public Restaurant Restaurant { get; set; }
        public int? RestaurantId { get; set; }

    }
}
