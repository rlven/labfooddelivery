﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LabWork62FoodDelivery.Models
{
    public class Dish
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public Restaurant Restaurant { get; set; }
        public int RestaurantId { get; set; }
        public Basket Basket { get; set; }
    }
}
