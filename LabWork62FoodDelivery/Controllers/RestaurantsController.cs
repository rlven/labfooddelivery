﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LabWork62FoodDelivery.Data;
using LabWork62FoodDelivery.Models;
using System.IO;
using LabWork62FoodDelivery.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.Operations;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace LabWork62FoodDelivery.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RestaurantsController(ApplicationDbContext context, FileUploadService fileUploadService, IHostingEnvironment evironment,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _fileUploadService = fileUploadService;
            _environment = evironment;
            _httpContextAccessor = httpContextAccessor;
        }

        // GET: Restaurants
        public async Task<IActionResult> Index()
        {
            return View(await _context.Restaurants.ToListAsync());
        }

        // GET: Restaurants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurants.Include(r=>r.Dishes).Include(r=>r.Baskets)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // GET: Restaurants/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Restaurants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Photo,Description")] Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                var path = Path.Combine(
                    _environment.WebRootPath,
                    $"images\\{restaurant.Name}\\avatar");
                _fileUploadService.Upload(path, restaurant.Photo.FileName, restaurant.Photo);
                restaurant.PhotoPath = $"images/{restaurant.Name}/avatar/{restaurant.Photo.FileName}";
                _context.Add(restaurant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(restaurant);
        }

        // GET: Restaurants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return NotFound();
            }
            return View(restaurant);
        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,PhotoPath,Description")] Restaurant restaurant)
        {
            if (id != restaurant.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restaurant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantExists(restaurant.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(restaurant);
        }

        // GET: Restaurants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurants
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _context.Restaurants.FindAsync(id);
            _context.Restaurants.Remove(restaurant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurants.Any(e => e.Id == id);
        }
        public async Task<IActionResult> AddToBasket(int id)
        {
            bool isContain = false;
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var dish = _context.Dishes.Find(id);
            var currRestaurantId = dish.RestaurantId;
            var currRestaurant = await _context.Restaurants.FindAsync(currRestaurantId);
            List<Basket> baskets = _context.Baskets.Where(b => b.RestaurantId == currRestaurantId).ToList();
            if (baskets.Count==0)
            {
                Basket basket = new Basket()
                {
                    UserId = userId,
                    Count = 1,
                    Price = dish.Price,
                    DishId = dish.Id,
                    RestaurantId = currRestaurantId
                };
                _context.Add(basket);
                await _context.SaveChangesAsync();
            }
            else
            {
                foreach (var bas in baskets)
                {
                    if (bas.DishId == dish.Id)
                    {
                        bas.Count++;
                        bas.Price += dish.Price;
                        isContain = true;
                        _context.Update(bas);
                        await _context.SaveChangesAsync();
                    }                    
                }
                if (isContain == false)
                {
                    Basket basket = new Basket()
                    {
                        UserId = userId,
                        Count = 1,
                        Price = dish.Price,
                        DishId = dish.Id,
                        RestaurantId = currRestaurantId
                    };
                    _context.Add(basket);
                    await _context.SaveChangesAsync();
                }
            }

            return RedirectToAction("Details", new { id=currRestaurantId });
        }
    }
}
