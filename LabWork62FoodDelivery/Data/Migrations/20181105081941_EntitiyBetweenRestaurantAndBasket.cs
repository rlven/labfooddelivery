﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LabWork62FoodDelivery.Data.Migrations
{
    public partial class EntitiyBetweenRestaurantAndBasket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RestaurantId",
                table: "Baskets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Baskets_RestaurantId",
                table: "Baskets",
                column: "RestaurantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Baskets_Restaurants_RestaurantId",
                table: "Baskets",
                column: "RestaurantId",
                principalTable: "Restaurants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Baskets_Restaurants_RestaurantId",
                table: "Baskets");

            migrationBuilder.DropIndex(
                name: "IX_Baskets_RestaurantId",
                table: "Baskets");

            migrationBuilder.DropColumn(
                name: "RestaurantId",
                table: "Baskets");
        }
    }
}
