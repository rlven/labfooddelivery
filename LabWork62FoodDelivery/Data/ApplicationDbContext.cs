﻿using System;
using System.Collections.Generic;
using System.Text;
using LabWork62FoodDelivery.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;
using Microsoft.EntityFrameworkCore;

namespace LabWork62FoodDelivery.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Basket> Baskets { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Restaurant>()
                .HasMany(r => r.Dishes)
                .WithOne(r => r.Restaurant)
                .HasPrincipalKey(r => r.Id);
            builder.Entity<Dish>()
                .HasOne(d => d.Restaurant)
                .WithMany(d => d.Dishes)
                .HasForeignKey(r => r.RestaurantId);
            builder.Entity<Basket>()
                .HasOne(b => b.Dish)
                .WithOne(b => b.Basket)
                .HasForeignKey<Basket>(b => b.DishId);
            builder.Entity<Basket>()
                .HasOne(b => b.User)
                .WithMany(b => b.Baskets)
                .HasForeignKey(u => u.UserId);
            builder.Entity<Restaurant>()
                .HasMany(r => r.Baskets)
                .WithOne(b => b.Restaurant)
                .HasPrincipalKey(r => r.Id);
            builder.Entity<Basket>()
                .HasOne(b => b.Restaurant)
                .WithMany(r => r.Baskets)
                .HasForeignKey(r => r.RestaurantId);
        }
    }
}
